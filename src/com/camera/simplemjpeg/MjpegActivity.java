package com.camera.simplemjpeg;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.WindowManager;
import android.widget.TextView;

import com.bender.hates.you.R;
import com.camera.simplemjpeg.joystick.DualJoystickView;
import com.camera.simplemjpeg.joystick.JoystickMovedListener;

public class MjpegActivity extends Activity implements SensorEventListener {
	private static final boolean DEBUG = false;
	private static final String TAG = "MJPEG";
	DualJoystickView joystick;
	private static int MAX_MSG = 4;
	private Queue<String> msg_queue;
	private List<Socket> sockets;
	private final String STOP_COMMAND="h";

	private MjpegView mv = null;
	String URL;

	// for settings (network and resolution)
	private static final int REQUEST_SETTINGS = 0;
	private static final float MAX_CAMERA_RANGE = 100;

	private int width = 320;
	private int height = 240;

	private int ip_ad1 = 192;
	private int ip_ad2 = 168;
	private int ip_ad3 = 0;
	private int ip_ad4 = 250;
	private int ip_port = 8080;
	private int counter = 0;
	private float[] mGravity;
	private float[] mGeomagnetic;
	private String ip_command = "?action=stream";

	private boolean suspending = false;

	private String POSTURL;
	private MjpegActivity context;

	private final int MAX_JOYSTICK = 55;
	private TextView connection;
	private boolean isConnected = false;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		msg_queue = new LinkedBlockingQueue<String>();
		sockets = new LinkedList<Socket>();
		SharedPreferences preferences = getSharedPreferences("SAVED_VALUES",
				MODE_PRIVATE);
		width = preferences.getInt("width", width);
		height = preferences.getInt("height", height);
		ip_ad1 = preferences.getInt("ip_ad1", ip_ad1);
		ip_ad2 = preferences.getInt("ip_ad2", ip_ad2);
		ip_ad3 = preferences.getInt("ip_ad3", ip_ad3);
		ip_ad4 = preferences.getInt("ip_ad4", ip_ad4);
		ip_port = preferences.getInt("ip_port", ip_port);
		ip_command = preferences.getString("ip_command", ip_command);

		StringBuilder sb = new StringBuilder();
		String s_http = "http://";
		String s_dot = ".";
		String s_colon = ":";
		String s_slash = "/";
		sb.append(s_http);
		sb.append(ip_ad1);
		sb.append(s_dot);
		sb.append(ip_ad2);
		sb.append(s_dot);
		sb.append(ip_ad3);
		sb.append(s_dot);
		sb.append(ip_ad4);
		POSTURL = new String("http://192.168.0.200:2000");
		POSTURL = POSTURL + "/cgi-bin/serial.cgi";
		sb.append(s_colon);
		sb.append(ip_port);
		sb.append(s_slash);

		sb.append(ip_command);
		URL = new String(sb);

		setContentView(R.layout.main);
		mv = (MjpegView) findViewById(R.id.mv);
		if (mv != null) {
			mv.setResolution(width, height);
			mv.setDisplayMode(MjpegView.SIZE_FULLSCREEN);
		}
		new DoRead().execute(URL);
		joystick = (DualJoystickView) findViewById(R.id.dualjoystickView);
		joystick.setMovementRange(MAX_CAMERA_RANGE, MAX_JOYSTICK);
		joystick.setYAxisInverted(true, false);
		joystick.setMoveResolution(10f, 20f);
		joystick.setOnJostickMovedListener(_cameraListener, _listener);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		connection = (TextView) findViewById(R.id.connection);

	}

	private JoystickMovedListener _cameraListener = new JoystickMovedListener() {

		private int ccounter;

		@Override
		public void OnReturnedToCenter() {
			// TODO Auto-generated method stub
			addToQueue("H" + Integer.toString(135));
			addToQueue("v" + Integer.toString(78));
		}

		@Override
		public void OnReleased() {
			addToQueue("H" + Integer.toString(135));
			addToQueue("v" + Integer.toString(78));

		}

		@Override
		public void OnMoved(int x, int y) {

			Log.v(TAG, "x" + Integer.toString((int) x));
			Log.v(TAG, "y" + Integer.toString((int) y));

			double h = (135 + ((25) * x / 100));
			double v = (77 + ((72) * y / 100));

			if (ccounter == 0) {
				addToQueue("H" + Integer.toString((int) h));
				addToQueue("v" + Integer.toString((int) v));
				ccounter++;
			} else {
				addToQueue("v" + Integer.toString((int) v));
				addToQueue("H" + Integer.toString((int) h));
				ccounter--;
			}
			if (link == null) {
				Log.v(TAG, "link:null");
				link = new sendLink();
				link.setActivity(context);
				link.execute("");
			}
		}
	};
	private JoystickMovedListener _listener = new JoystickMovedListener() {

		@Override
		public void OnMoved(int direction, int velocitiy) {
			// Log.v(TAG, "l" + Integer.toString(pan));
			// Log.v(TAG, "r" + Integer.toString(tilt));

			double rCoeff = 1;
			double lCoeff = 1;

			if (direction < 0) {
				lCoeff = ((double) -MAX_JOYSTICK - direction)
						/ (double) -MAX_JOYSTICK;
			} else if (direction > 0) {
				rCoeff = ((double) MAX_JOYSTICK - direction)
						/ (double) MAX_JOYSTICK;
			}
			int r = (int) (velocitiy * rCoeff);
			int l = (int) (velocitiy * lCoeff);
			if (r < 0) {
				r -= 200;
			} else {
				r += 200;
			}
			if (l < 0) {
				l -= 200;
			} else {
				l += 200;
			}
			if(velocitiy<10 && velocitiy>-10 && direction>25){
				l=r*-1;
			}
			if(velocitiy<10 && velocitiy>-10 && direction<-25){
			
				r=l*-1;
			}
			// Log.v(TAG,"lCoeff: "+lCoeff+"");
			// Log.v(TAG,"rCoeff: "+rCoeff+"");
			// r=
			// if (direction < 0) {
			// direction -= 200;
			// } else {
			// direction += 200;
			// }
			if (counter == 0) {
				addToQueue("r" + Integer.toString(r));
				addToQueue("l" + Integer.toString(l));
				counter++;
			} else {
				addToQueue("l" + Integer.toString(l));
				addToQueue("r" + Integer.toString(r));
				counter--;
			}
			if (link == null) {
				Log.v(TAG, "link:null");
				link = new sendLink();
				link.setActivity(context);
				link.execute("");
			}

		}

		@Override
		public void OnReleased() {
			//msg_queue.clear();
			addToQueue(STOP_COMMAND);
			addToQueue(STOP_COMMAND);
			addToQueue(STOP_COMMAND);

		}

		public void OnReturnedToCenter() {
			addToQueue(STOP_COMMAND);

		};
	};

	public void addToQueue(String msg) {
		if (msg_queue.size() == MAX_MSG) {
			msg_queue.remove();
			msg_queue.add(msg);
		} else {
			msg_queue.add(msg);
		}
	}

	public void onResume() {
		if (DEBUG)
			Log.d(TAG, "onResume()");
		super.onResume();
		if (mv != null) {
			if (suspending) {
				new DoRead().execute(URL);
				suspending = false;
			}
		}
		// mSensorManager.registerListener(this, mAccelerometer,
		// SensorManager.SENSOR_DELAY_NORMAL);
		// mSensorManager.registerListener(this, mMagnetomer,
		// SensorManager.SENSOR_DELAY_NORMAL);
		setConnected(false);
	}

	public void onStart() {
		if (DEBUG)
			Log.d(TAG, "onStart()");
		super.onStart();

	}

	private sendLink link;

	private class sendLink extends AsyncTask<String, String, Boolean> {

		private MjpegActivity activity;
		Socket socket = null;

		@Override
		protected Boolean doInBackground(String... params) {

			openSocket();
			if (socket != null) {
				publishProgress("true");
				String path = "/cgi-bin/serial.cgi";
				BufferedWriter wr = null;
				// BufferedReader rd = null;
				try {
					wr = new BufferedWriter(new OutputStreamWriter(
							socket.getOutputStream(), "UTF8"));
					// rd = new BufferedReader(new InputStreamReader(
					// socket.getInputStream()));
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				while (!isCancelled()) {
					if (msg_queue.size() > 0) {
						Log.v(TAG, "sending: " + msg_queue.element());
//						if (msg_queue.element().equalsIgnoreCase("h")) {
//							msg_queue.add("h");
//						}

						try {
							//msg_queue.remove();
							wr.write(msg_queue.remove() + "\r\n");
//							if (msg_queue.size() == 0) {
//								// msg_queue.add("z");
//
//							}
							wr.flush();

						} catch (SocketException e) {
							publishProgress("false");
							e.printStackTrace();
							openSocket();
						} catch (Exception e) {
							e.printStackTrace();
						}
						SystemClock.sleep(50);
					}
				}

				try {
					wr.close();
				} catch (IOException e) {

				}
			}
			return null;

		}

		@Override
		protected void onProgressUpdate(String... values) {

			activity.setConnected(Boolean.parseBoolean(values[0]));
			super.onProgressUpdate(values);
		}

		public void openSocket() {
			String authorizationString = "Basic "
					+ Base64.encodeToString(("root" + ":" + "1").getBytes(),
							Base64.DEFAULT); // this line is diffe
			try {
				int port = 80;
				// int port=2000;
				Log.v(TAG, "openSocket");
				socket = new Socket("192.168.0.200", 2000);
				sockets.add(socket);
				// socket = new Socket("192.168.0.18", port);

				Log.v(TAG, "openedSocket");

			} catch (UnknownHostException e2) {
				e2.printStackTrace();
			} catch (IOException e2) {
				e2.printStackTrace();
			}

		}

		public void setActivity(MjpegActivity baseContext) {
			activity = (MjpegActivity) baseContext;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				socket.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
			activity.link = null;
			activity.setConnected(false);
			activity = null;
		}

	}

	private void setConnected(boolean state) {
		if (state) {
			connection.setTextColor(Color.parseColor("#FF669900"));
			connection.setText(getString(R.string.connected));
		} else {
			connection.setTextColor(Color.parseColor("#FFCC0000"));
			connection.setText(getString(R.string.not_connected));

		}
	}

	public void onPause() {
		if (DEBUG)
			Log.d(TAG, "onPause()");
		super.onPause();
		if (mv != null) {
			if (mv.isStreaming()) {
				mv.stopPlayback();
				suspending = true;
			}
		}

		if (link != null) {
			link.cancel(true);
			msg_queue = new LinkedList<String>();
		}
		Iterator<Socket> iter = sockets.iterator();
		// int i = 0;
		while (iter.hasNext()) {
			Socket it = iter.next();
			if (it != null) {
				try {
					it.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// sockets.remove(i++);
		}

	}

	public void onStop() {
		if (DEBUG)
			Log.d(TAG, "onStop()");
		super.onStop();
	}

	public void onDestroy() {
		if (DEBUG)
			Log.d(TAG, "onDestroy()");

		if (mv != null) {
			mv.freeCameraMemory();
		}
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.option_menu, menu);
		return true;
	}

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.settings:
	// Intent settings_intent = new Intent(MjpegActivity.this,
	// SettingsActivity.class);
	// settings_intent.putExtra("width", width);
	// settings_intent.putExtra("height", height);
	// settings_intent.putExtra("ip_ad1", ip_ad1);
	// settings_intent.putExtra("ip_ad2", ip_ad2);
	// settings_intent.putExtra("ip_ad3", ip_ad3);
	// settings_intent.putExtra("ip_ad4", ip_ad4);
	// settings_intent.putExtra("ip_port", ip_port);
	// settings_intent.putExtra("ip_command", ip_command);
	// startActivityForResult(settings_intent, REQUEST_SETTINGS);
	// return true;
	// }
	// return false;
	// }

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_SETTINGS:
			if (resultCode == Activity.RESULT_OK) {
				width = data.getIntExtra("width", width);
				height = data.getIntExtra("height", height);
				ip_ad1 = data.getIntExtra("ip_ad1", ip_ad1);
				ip_ad2 = data.getIntExtra("ip_ad2", ip_ad2);
				ip_ad3 = data.getIntExtra("ip_ad3", ip_ad3);
				ip_ad4 = data.getIntExtra("ip_ad4", ip_ad4);
				ip_port = data.getIntExtra("ip_port", ip_port);
				ip_command = data.getStringExtra("ip_command");

				if (mv != null) {
					mv.setResolution(width, height);
				}
				SharedPreferences preferences = getSharedPreferences(
						"SAVED_VALUES", MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putInt("width", width);
				editor.putInt("height", height);
				editor.putInt("ip_ad1", ip_ad1);
				editor.putInt("ip_ad2", ip_ad2);
				editor.putInt("ip_ad3", ip_ad3);
				editor.putInt("ip_ad4", ip_ad4);
				editor.putInt("ip_port", ip_port);
				editor.putString("ip_command", ip_command);

				editor.commit();

				new RestartApp().execute();
			}
			break;
		}
	}

	public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
		protected MjpegInputStream doInBackground(String... url) {
			// TODO: if camera has authentication deal with it and don't just
			// not work
			HttpResponse res = null;
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpParams httpParams = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 5 * 1000);
			Log.d(TAG, "1. Sending http request");
			try {
				res = httpclient.execute(new HttpGet(URI.create(url[0])));
				Log.d(TAG, "2. Request finished, status = "
						+ res.getStatusLine().getStatusCode());
				if (res.getStatusLine().getStatusCode() == 401) {
					// You must turn off camera User Access Control before this
					// will work
					return null;
				}
				return new MjpegInputStream(res.getEntity().getContent());
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				Log.d(TAG, "Request failed-ClientProtocolException", e);
				// Error connecting to camera
			} catch (IOException e) {
				e.printStackTrace();
				Log.d(TAG, "Request failed-IOException", e);
				// Error connecting to camera
			}
			return null;
		}

		protected void onPostExecute(MjpegInputStream result) {
			mv.setSource(result);
			if (result != null)
				result.setSkip(1);
			mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
			mv.showFps(false);
		}
	}

	public class RestartApp extends AsyncTask<Void, Void, Void> {
		protected Void doInBackground(Void... v) {
			MjpegActivity.this.finish();
			return null;
		}

		protected void onPostExecute(Void v) {
			startActivity((new Intent(MjpegActivity.this, MjpegActivity.class)));
		}
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			mGravity = event.values;
		}
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			mGeomagnetic = event.values;
		}
		if (mGravity != null && mGeomagnetic != null) {
			float R[] = new float[9];
			float I[] = new float[9];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity,
					mGeomagnetic);
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(R, orientation);
				float x = orientation[0];
				double unit = 1;
				Log.v(TAG, "x:" + x * unit);
				float y = orientation[1];
				Log.v(TAG, "y:" + y * unit);
				float z = orientation[2];
				Log.v(TAG, "z:" + z * unit);
				// at this point, orientation contains the azimut, pitch and
				// roll values.
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	// public void onSensorChanged(SensorEvent event) {
	// float x = event.values[0];
	// double unit=1;
	// Log.v(TAG,"x:"+x*unit);
	// float y = event.values[1];
	// Log.v(TAG,"y:"+y*unit);
	// float z = event.values[2];
	// Log.v(TAG,"z:"+z*unit);
	// }
}
