package com.camera.simplemjpeg.joystick;

public interface JoystickClickedListener {
	public void OnClicked();
	public void OnReleased();
}
